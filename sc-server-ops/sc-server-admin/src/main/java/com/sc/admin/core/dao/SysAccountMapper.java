package com.sc.admin.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.account.SysAccount;

/**
 * @author: wust
 * @date: 2019-12-27 11:55:31
 * @description:
 */
public interface SysAccountMapper extends IBaseMapper<SysAccount>{
}