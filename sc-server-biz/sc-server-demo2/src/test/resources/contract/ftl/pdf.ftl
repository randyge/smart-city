<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Title</title>
    <style mce_bogus="1" type="text/css">
        .template {
            font-family: "SimSun";
            color: black;
            padding: 10px 40px 10px 40px;
        }
        .template div {
            line-height: 1.5;
        }
        .header1 {
            font-size: 20px;
            font-weight: 800;
            text-align: center
        }
        .header2 {
            font-family: "SimSun";
            font-size: 15px;
            font-weight: 800;
            text-align: center
        }

        .table1 table{
            line-height: 1;
            margin-top: 5px;
            width: 100%;
            border : 0.5px solid black;
            border: 0.5px solid black;
            table-layout:fixed;
            border-collapse: collapse;
            overflow:hidden;
        }
        .table1 td{
            text-align:center;
            border: 0.5px solid black;
            border: 0.5px solid black;
            word-break:break-all;
            border-collapse: collapse;
            font-size: 15px;
        }
    </style>
</head>
<body>
    <div id="templateNumFiv" class="template">
        <p class="header1">${(companyFullName)?default("")}</p>
        <p class="header2">劳动协议</p><br/><br/><br/>
        <table width="100%">
            <tr>
                <td width="70%" style="text-align: left;font-size: 15px;">张三</td>
                <td width="30%" style="text-align: left;font-size: 15px;">编号：22323232</td>
            </tr>
        </table>
        <table class="table1" style="width: 100%;table-layout:fixed;word-break:break-all;padding-bottom: 0px;margin-bottom: 0px;border-bottom: 0px;border-collapse:collapse;" >
            <tr>
                <td style="width: 10%;text-align: center;" >买方</td>
                <td style="width: 45%;text-align: center;" >张三</td>
                <td style="width: 15%;text-align: center;">合同号</td>
                <td style="width: 30%;text-align: center;" >${(conNum)?default("")}</td>
            </tr>
        </table>
        <br/>
        <table width="100%" style="">
            <tr>
                <td style="width:60%;font-size: 15px;"></td>
                <td style="width:40%;text-align: center;font-size: 15px;">${(companyFullName)?default("")}</td>
            </tr>
            <tr>
                <td style="width:60%;font-size: 15px;"></td>
                <td style="width:40%;text-align: center;font-size: 15px;">${(orderDate)?default("")}</td>
            </tr>
        </table><br/><br/><br/><br/><br/><br/>

        <table width="100%">
            <tr>
                <td style="width:33%;text-align: left;font-size: 15px;">部门经理：小吴</td>
                <td style="width:33%;text-align: left;font-size: 15px;">经办人：${(peopleName)?default("")}</td>
            </tr>
        </table>
    </div>
</body>
</html>