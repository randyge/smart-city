package com.sc.order.core.service;

import com.alibaba.fastjson.JSONObject;
import com.sc.common.dto.WebResponseDto;

/**
 * @author: wust
 * @date: 2020-07-15 14:16:53
 * @description:
 */
public interface OrderDetailImportService {
    WebResponseDto importByExcel(JSONObject jsonObject);
}
