package com.sc.common.exception;

public interface ReturnCodeAware {
    String getErrorCode();
    String getErrorMessage();
    Object[] getArgs();
}
