package com.sc.admin.core.service;

import org.springframework.dao.DataAccessException;
import java.util.List;
import java.util.Map;

public interface CommonService {
    List<Map<String, Object>> findBySql(Map<String, Object> parameters) throws DataAccessException;

    /**
     * 清理代理商相关所有数据，必须是开发环境才能成功执行此方法
     * @param projectId
     */
    void cleaningProjectAllData(Long projectId);
}
