package com.sc.admin.api;

import com.sc.admin.api.fallback.NotificationServiceFallback;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.notification.SysNotification;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "admin-server",fallback = NotificationServiceFallback.class)
public interface NotificationService {
    String API_PREFIX = "/api/feign/v1/NotificationFeignApi";
    String CREATE = API_PREFIX + "/create";

    @RequestMapping(value = CREATE,method= RequestMethod.POST, consumes = "application/json")
    WebResponseDto create(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestBody SysNotification entity);
}
