package com.sc.workorder.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.workorder.entity.WoWorkOrderUser;

/**
 * @author: wust
 * @date: 2020-04-01 10:18:23
 * @description:
 */
public interface WoWorkOrderUserMapper extends IBaseMapper<WoWorkOrderUser>{
}