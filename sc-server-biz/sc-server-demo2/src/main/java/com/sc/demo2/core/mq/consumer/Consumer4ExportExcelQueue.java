package com.sc.demo2.core.mq.consumer;

import com.alibaba.fastjson.JSONObject;
import com.sc.admin.api.ImportExportService;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.importexport.SysImportExport;
import com.sc.common.entity.admin.importexport.SysImportExportSearch;
import com.sc.common.exception.BusinessException;
import com.sc.common.service.ExportExcelService;
import com.sc.common.util.MyStringUtils;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import java.util.Date;

/**
 * @author ：wust
 * @date ：Created in 2019/7/19 15:21
 * @description：
 * @version:
 */
@Component
@RabbitListener(
        containerFactory = "singleListenerContainer",
        bindings = @QueueBinding(
                value = @Queue(
                        value = "${spring.rabbitmq.exportexcel.queue.name}",
                        durable = "${spring.rabbitmq.exportexcel.queue.durable}"
                ),
                exchange = @Exchange(
                        value = "${spring.rabbitmq.exportexcel.exchange.name}",
                        durable = "${spring.rabbitmq.exportexcel.exchange.durable}",
                        type = "${spring.rabbitmq.exportexcel.exchange.type}",
                        ignoreDeclarationExceptions = "${spring.rabbitmq.exportexcel.exchange.ignoreDeclarationExceptions}"
                ),
                key = "${spring.rabbitmq.exportexcel.routing-key}"
        )
)
public class Consumer4ExportExcelQueue {
    @Autowired
    private ExportExcelService exportExcelServiceImpl;

    @Autowired
    private ImportExportService importExportService;

    @Autowired
    private Environment environment;

    @RabbitHandler
    public void process(JSONObject jsonObject) {
        WebResponseDto responseDto = new WebResponseDto();

        String applicationName = jsonObject.getString("spring.application.name");
        String applicationName1 = environment.getProperty("spring.application.name");

        if(MyStringUtils.isBlank(applicationName)){
            throw new BusinessException("请指定目标服务名");
        }


        if(!applicationName.equals(applicationName1)){
            return;
        }

        try {
            this.before(jsonObject);
            responseDto = this.doExport(jsonObject);
        } catch (Exception e) {
            responseDto.setCode("A100504");
            if (MyStringUtils.isNotBlank(e.getMessage())) {
                int length = e.getMessage().length() >= 500 ? 500 : e.getMessage().length();
                responseDto.setMessage(e.getMessage().substring(0, length));
            } else {
                int length = e.toString().length() >= 500 ? 500 : e.toString().length();
                responseDto.setMessage("导出失败:" + e.toString().substring(0, length));
            }
        } finally {
            this.after(jsonObject, responseDto);
        }
    }


    /**
     * 导出前，记录初始状态和开始时间
     *
     * @param jsonObject
     */
    private void before(JSONObject jsonObject) {
        DefaultBusinessContext ctx = jsonObject.getObject("ctx",DefaultBusinessContext.class);
        SysImportExport sysImportExport = jsonObject.getObject("sysImportExport", SysImportExport.class);
        SysImportExport importExportSearch = new SysImportExport();
        importExportSearch.setBatchNo(sysImportExport.getBatchNo());
        WebResponseDto responseDto  = importExportService.selectOne(JSONObject.toJSONString(ctx),importExportSearch);
        if(!responseDto.isSuccess()){
            throw new BusinessException(responseDto.getMessage());
        }

        SysImportExport importExport = JSONObject.parseObject(JSONObject.toJSONString(responseDto.getObj()),SysImportExport.class);
        if(importExport == null){
            WebResponseDto responseDto1 = importExportService.insert(JSONObject.toJSONString(ctx),sysImportExport);
            if(!responseDto1.INFO_SUCCESS.equals(responseDto1.getFlag())){
                throw new BusinessException(responseDto1.getMessage());
            }
        }
    }

    /**
     * 执行导出
     *
     * @param jsonObject
     */
    private WebResponseDto doExport(JSONObject jsonObject) {
        return exportExcelServiceImpl.export(jsonObject);
    }

    /**
     * 导出后，记录结果状态，并记录结束时间
     *
     * @param jsonObject
     * @param responseDto
     */
    private void after(JSONObject jsonObject, WebResponseDto responseDto) {
        DefaultBusinessContext ctx = jsonObject.getObject("ctx",DefaultBusinessContext.class);

        SysImportExport sysImportExport = jsonObject.getObject("sysImportExport", SysImportExport.class);
        SysImportExportSearch condition = new SysImportExportSearch();
        condition.setBatchNo(sysImportExport.getBatchNo());
        WebResponseDto responseDto1 = importExportService.selectOne(JSONObject.toJSONString(ctx),condition);
        if(!WebResponseDto.INFO_SUCCESS.equals(responseDto1.getFlag())){
            throw new BusinessException("导出失败");
        }

        SysImportExport importExport = JSONObject.parseObject(JSONObject.toJSONString(responseDto1.getObj()),SysImportExport.class);
        if(!responseDto.isSuccess()){
            if (importExport != null) {
                importExport.setStatus(responseDto.getCode());
                importExport.setEndTime(new Date());
                importExport.setModifyTime(new Date());
                importExport.setMsg(responseDto.getMessage());
                importExportService.updateByPrimaryKeySelective(JSONObject.toJSONString(ctx),importExport);
            }
        }else{
            if(importExport != null){
                JSONObject jsonObjectResponse = (JSONObject) responseDto.getObj();
                importExport.setStatus(responseDto.getCode());
                importExport.setEndTime(new Date());
                importExport.setModifyTime(new Date());
                importExport.setMsg(responseDto.getMessage());
                importExport.setName(jsonObjectResponse == null ? null : jsonObjectResponse.getString("name"));
                importExport.setSize(jsonObjectResponse == null ? null : jsonObjectResponse.getInteger("size"));
                importExport.setFileId(jsonObjectResponse == null ? null : jsonObjectResponse.getLong("fileId"));
                responseDto = importExportService.updateByPrimaryKeySelective(JSONObject.toJSONString(ctx),importExport);
                if(!responseDto.isSuccess()){
                    throw new BusinessException("导出失败");
                }
            }
        }
    }
}
