package com.sc.common.entity.admin.notification;

import com.sc.common.entity.BaseEntity;

/**
 * @author: wust
 * @date: 2020-04-09 13:26:08
 * @description:
 */
public class SysNotificationTimeline extends BaseEntity {
        private Long notificationId;
    private String relationCode;
    private String type;
    private String description;
                        

        public void setNotificationId (Long notificationId) {this.notificationId = notificationId;} 
    public Long getNotificationId(){ return notificationId;} 
    public void setRelationCode (String relationCode) {this.relationCode = relationCode;} 
    public String getRelationCode(){ return relationCode;} 
    public void setType (String type) {this.type = type;} 
    public String getType(){ return type;} 
    public void setDescription (String description) {this.description = description;} 
    public String getDescription(){ return description;} 
                        
}