package com.sc.admin.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.mynotice.SysMyNotice;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

public interface SysMyNoticeMapper extends IBaseMapper<SysMyNotice> {
    List<Map> getUnreadNoticeCount(@Param("status") String status, @Param("receiver") String receiver);
}