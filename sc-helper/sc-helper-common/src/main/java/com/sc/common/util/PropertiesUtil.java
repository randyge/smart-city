package com.sc.common.util;

import com.sc.common.exception.BusinessException;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.IOException;
import java.util.Properties;

public class PropertiesUtil {
    private PropertiesUtil(){
    }

    public static Properties getDataSourceCompanyMapProperties(){
        ResourcePatternResolver resourceLoader = new PathMatchingResourcePatternResolver();
        Resource resource = resourceLoader.getResource("classpath:/datasource-company-map.propertis");
        Properties properties = new Properties();
        try {
            properties.load(resource.getInputStream());
        } catch (IOException e) {
            throw new BusinessException("读取配置[datasource-company-map.propertis]失败");
        }
        return properties;
    }

}
