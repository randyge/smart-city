package com.sc.demo1.api;


import com.sc.common.dto.WebResponseDto;
import com.sc.demo1.api.fallback.DemoServiceFallback;
import com.sc.demo1.entity.demo.Demo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "demo1-server",fallback = DemoServiceFallback.class)
public interface DemoService {
    String API_PREFIX = "/api/feign/v1/DemoFeignApi";
    String SELECT = API_PREFIX + "/select";

    @RequestMapping(value = SELECT, method = RequestMethod.POST, consumes = "application/json")
    WebResponseDto select(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestBody Demo search);
}
