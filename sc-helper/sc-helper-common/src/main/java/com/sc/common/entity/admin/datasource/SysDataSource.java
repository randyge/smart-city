package com.sc.common.entity.admin.datasource;

import com.sc.common.annotations.EnableDistributedCaching;
import com.sc.common.annotations.EnableLocalCaching;
import com.sc.common.entity.BaseEntity;


/**
 * table name:  sys_data_source
 * author name: wust
 * create time: 2019-06-17 15:42:21
 */
@EnableLocalCaching
@EnableDistributedCaching
public class SysDataSource extends BaseEntity {
	private static final long serialVersionUID = 5502135893804008206L;

	private String name;
	private String groupName;
	private String masterSlave;
	private String dbType;
	private String jdbcUrl;
	private String jdbcUsername;
	private String jdbcPassword;
	private String jdbcDriver;
	private String description;
	private String shardingColumn;
	private Long projectId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getMasterSlave() {
		return masterSlave;
	}

	public void setMasterSlave(String masterSlave) {
		this.masterSlave = masterSlave;
	}

	public String getDbType() {
		return dbType;
	}

	public void setDbType(String dbType) {
		this.dbType = dbType;
	}

	public String getJdbcUrl() {
		return jdbcUrl;
	}

	public void setJdbcUrl(String jdbcUrl) {
		this.jdbcUrl = jdbcUrl;
	}

	public String getJdbcUsername() {
		return jdbcUsername;
	}

	public void setJdbcUsername(String jdbcUsername) {
		this.jdbcUsername = jdbcUsername;
	}

	public String getJdbcPassword() {
		return jdbcPassword;
	}

	public void setJdbcPassword(String jdbcPassword) {
		this.jdbcPassword = jdbcPassword;
	}

	public String getJdbcDriver() {
		return jdbcDriver;
	}

	public void setJdbcDriver(String jdbcDriver) {
		this.jdbcDriver = jdbcDriver;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getShardingColumn() {
		return shardingColumn;
	}

	public void setShardingColumn(String shardingColumn) {
		this.shardingColumn = shardingColumn;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
}

